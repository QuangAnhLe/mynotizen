/**
 * AppointmentListAdapter to render the data from db
 */

package com.example.quangale.mynotizen;

import android.content.ContextWrapper;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.MyViewHolder> {
    public class MyViewHolder extends RecyclerView.ViewHolder {
        FrameLayout frameLayout;
        public TextView date, timeBegin, timeEnd, title, dayMonth, place, color;

        public MyViewHolder(View view) {
            super(view);
            //TODO Framelayout im XML einarbeiten
            //frameLayout = (FrameLayout)view.findViewById(R.id.frameLayout);
            date = view.findViewById(R.id.dayMonth);
            title = view.findViewById(R.id.title);
            timeBegin = view.findViewById(R.id.timeBegin);
            timeEnd = view.findViewById(R.id.timeEnd);
            dayMonth = view.findViewById(R.id.dayMonth);
            place = view.findViewById(R.id.place);
            color = view.findViewById(R.id.color);
        }

        void update(final long _id) {
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ((AppCompatActivity) view.getContext()).startSupportActionMode(actionModeCallbacks);
                    selectItem(_id);
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectItem(_id);
                }
            });
        }
    }


    public AppointmentListAdapter(List<AppointmentsObject> _appointmentsObjectList) {
        this.appointmentsObjectList = _appointmentsObjectList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_row_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        appointment = appointmentsObjectList.get(position);
        holder.dayMonth.setText((appointment.getDate()));
        holder.title.setText(appointment.getTitle());
        holder.timeBegin.setText((appointment.getDateBegin()));
        holder.timeEnd.setText((appointment.getDateEnd()));
        holder.place.setText(appointment.getPlace());
        holder.color.setBackgroundColor(getColorById(appointment.getColorId()));

        //for longpress
        holder.update(appointment.getId());
    }

    void selectItem(long _id) {
        if (multiSelect) {
            if (selectedItems.contains(_id)) {
                selectedItems.remove(_id);
                //frameLayout.setBackgroundColor(Color.WHITE);
            } else {
                selectedItems.add(_id);
                //frameLayout.setBackgroundColor(Color.LTGRAY);
            }
        }
    }

    private ActionMode.Callback actionModeCallbacks = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            multiSelect = true;
            menu.add("Delete");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int index;
            for (int i = 0; i < selectedItems.size(); i++) {
                index = selectedItems.get(i).intValue() - 1;
                //TODO behebe nullpointer exeption
//                Log.d(LOG_TAG, selectedItems.get(i) + "");
//                Log.d(LOG_TAG, appointmentsObjectList.get(index).toString() + "");
                calLisActivity.deleteAppointment(appointmentsObjectList.get(index));
            }
            mode.finish();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            multiSelect = false;
            selectedItems.clear();
            notifyDataSetChanged();
        }
    };


   @Override
    public int getItemCount() {
        return appointmentsObjectList.size();
    }

    private String extractTime(long _time) {
        Date date = new Date(_time);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(date);
    }

    private String extractDayMonth(long _time) {
        Date date = new Date(_time);
        DateFormat formatter = new SimpleDateFormat("MMMM d (E)");
        return formatter.format(date);
    }


    private int getColorById(int _id) {
        switch (_id) {
            case 0:
                return Color.BLACK;
            case 1:
                return Color.RED;
            case 2:
                return Color.BLUE;
            default:
                return Color.GREEN;
        }
    }

    private View itemView;
    private boolean multiSelect = false;
    private ArrayList<Long> selectedItems = new ArrayList<Long>();
    AppointmentsObject appointment;
    CalendarListActivity calLisActivity = new CalendarListActivity();
    public static final String LOG_TAG = CalendarActivity.class.getSimpleName(); //Log tag for filtering
    private List<AppointmentsObject> appointmentsObjectList;
}
