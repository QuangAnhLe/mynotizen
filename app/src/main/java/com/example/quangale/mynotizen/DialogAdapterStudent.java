package com.example.quangale.mynotizen;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

class DialogAdapterStudent extends BaseAdapter {
    Activity activity;

    private Activity context;
    private ArrayList<DialogsObject> alCustom;
    private String sturl;


    public DialogAdapterStudent(Activity context, ArrayList<DialogsObject> alCustom) {
        this.context = context;
        this.alCustom = alCustom;

    }

    @Override
    public int getCount() {
        return alCustom.size();

    }

    @Override
    public Object getItem(int i) {
        return alCustom.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_addapter, null, true);

        TextView tvTitle= listViewItem.findViewById(R.id.tv_name);
        TextView tvSubject= listViewItem.findViewById(R.id.tv_type);
        TextView tvDuedate= listViewItem.findViewById(R.id.tv_desc);
        TextView tvDescription= listViewItem.findViewById(R.id.tv_class);


        tvTitle.setText("Title : "+alCustom.get(position).getTitles());
        tvSubject.setText("Subject : "+alCustom.get(position).getSubjects());
        tvDuedate.setText("Due Date : "+alCustom.get(position).getDuedates());
        tvDescription.setText("Description : "+alCustom.get(position).getDescripts());

        return  listViewItem;
    }

}


