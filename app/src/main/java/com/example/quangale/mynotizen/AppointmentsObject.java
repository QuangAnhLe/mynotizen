/**
 * This class representate an java object filled with data from the database
 */

package com.example.quangale.mynotizen;

import java.util.Date;

public class AppointmentsObject {

    public AppointmentsObject(long _id, String _date, String _dateBegin, String _dateEnd, boolean _isAllDay, String _title, String _place, int _colorId){
        id          = _id;
        date        = _date;
        dateBegin   = _dateBegin;
        dateEnd     = _dateEnd;
        isAllDay    = _isAllDay;
        title       = _title;
        place       = _place;
        colorId     = _colorId;
    }

    public long getId() { return id; }

    public String getPlace() { return place; }

    public String getDate() { return date;}

    public String getDateBegin() { return dateBegin; }

    public String getDateEnd() { return dateEnd; }

    public boolean isAllDay() { return isAllDay; }

    public String getTitle() { return title; }

    public int getColorId() { return colorId; }

    public void setTitle(String _title) { this.title = _title; }

    public void setDate(String _date){ this.date=_date;}

    public void setDateBegin(String _dateBegin) { this.dateBegin = _dateBegin; }

    public void setDateEnd(String _dateEnd) { this.dateEnd = _dateEnd; }

    public void setAllDay(boolean _isAllDay) { this.isAllDay = _isAllDay; }

    public void setId(long _id) { this.id = _id; }

    public void setPlace(String _place) { this.place = _place; }

    public void setColorId(int _colorId) { this.colorId = _colorId; }

    //Convert a calendar entry into a string
    @Override
    public String toString() {
        return id + ": " + new Date(dateBegin) + " bis " + new Date(dateEnd) + ": " + title + ", Ort: " + place;
    }

    private long    id;
    private String  dateBegin;
    private String  date;
    private String  dateEnd;
    private boolean isAllDay;
    private String  title;
    private String  place;
    private int     colorId; // 0 = Black, 1 = Red, 2 = Blue
}
