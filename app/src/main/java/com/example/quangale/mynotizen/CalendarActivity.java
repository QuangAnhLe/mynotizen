package com.example.quangale.mynotizen;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.DatePicker;
import android.widget.EditText;

import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Time;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import java.util.List;
import java.util.Locale;


public class CalendarActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Datenbankteil
    public static final String LOG_TAG = CalendarActivity.class.getSimpleName(); //Log tag for filtering
    private AppointmentsDataSource dataSource;
    //Datenbankteil

    CalendarView calendarView;
    TextView saveMyDate, switchView;

    private Button bDate;
    private Button mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListerner;
    private Button mDisplayTime_begin;
    private TimePickerDialog.OnTimeSetListener mTimeSetListerner_begin;
    private Button mDisplayTime_end;
    private TimePickerDialog.OnTimeSetListener mTimeSetListerner_end;
    private Switch mSwitchAllOffDay;
    private CheckBox checkboxBlack ;
    private CheckBox checkboxRed ;
    private CheckBox checkboxBlue ;
    private CheckBox checkboxGreen ;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        activateSavedButton();
        activeSwitch();
        selectItem();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,toolbar,
                R.string.navigation_draw_open, R.string.navigation_draw_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mDisplayDate = findViewById(R.id.calendar_date);
        mDisplayTime_begin = findViewById(R.id.calendar_time_begin);
        mDisplayTime_end = findViewById(R.id.calendar_time_end);
        mSwitchAllOffDay = findViewById(R.id.switchAllofDay);
        /*checkboxBlack = (CheckBox) findViewById(R.id.checkboxBlack);
        checkboxRed = (CheckBox) findViewById(R.id.checkboxRed);
        checkboxBlue = (CheckBox) findViewById(R.id.checkboxBlue);
        checkboxGreen = (CheckBox) findViewById(R.id.checkboxGreen);*/





        mDisplayDate.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(CalendarActivity.this, mDateSetListerner, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        // if don't use switch all of day
        // then we must choice date,
        // time begin and time end
        // in the buttons
        mDateSetListerner = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Log.d(TAG,"onDateSet: dd/mm/yyy"+ dayOfMonth + "." +(month+1)+"."+year);
                String dayString;
                if (dayOfMonth < 10)
                    dayString = "0" + dayOfMonth;
                else
                    dayString = "" +dayOfMonth;

                String monthString;
                month=month+1;
                if (month < 10)
                    monthString = "0" + month;
                else
                    monthString = "" + month;
                String date = dayString + "-" + monthString + "-" + year;
                mDisplayDate.setText(date);
            }
        };
        /*output time beginning*/
        mDisplayTime_begin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(CalendarActivity.this, mTimeSetListerner_begin, hour, minute, true);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });
        // set time in format hh:mm
        mTimeSetListerner_begin = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String hourString;
                if (hourOfDay < 10)
                    hourString = "0" + hourOfDay;
                else
                    hourString = "" +hourOfDay;

                String minuteSting;
                if (minute < 10)
                    minuteSting = "0" + minute;
                else
                    minuteSting = "" +minute;

                String time = hourString + ":" + minuteSting;

                mDisplayTime_begin.setText(time);
            }
        };
        /*choice time ending*/
        mDisplayTime_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                TimePickerDialog dialog = new TimePickerDialog(CalendarActivity.this, mTimeSetListerner_end, hour, minute, true);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });
        // for time in formal hh:mm
        mTimeSetListerner_end = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hourString;
                if (hourOfDay < 10)
                    hourString = "0" + hourOfDay;
                else
                    hourString = "" +hourOfDay;

                String minuteSting;
                if (minute < 10)
                    minuteSting = "0" + minute;
                else
                    minuteSting = "" +minute;

                String time = hourString + ":" + minuteSting;
                mDisplayTime_end.setText(time);
            }
        };

        //Intanciate a new dataSource-object
        dataSource = new AppointmentsDataSource(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch(menuItem.getItemId()){
            case R.id.nav_overview_day:
                Intent switchCalendarOverViewIntent;
                switchCalendarOverViewIntent = new Intent(CalendarActivity.this, CalendarOverViewActivity.class);
                switchCalendarOverViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(switchCalendarOverViewIntent);
                break;

            case R.id.nav_suchen:
                Intent switchCalendarSearchIntent2;
                switchCalendarSearchIntent2 = new Intent(CalendarActivity.this, CalendarSearchActivity.class);
                switchCalendarSearchIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(switchCalendarSearchIntent2);
                break;

            case R.id.nav_list_calendar:
                Intent switchCalendarViewIntent;
                switchCalendarViewIntent = new Intent(CalendarActivity.this, CalendarListActivity.class);
                switchCalendarViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(switchCalendarViewIntent);
                break;


        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer((GravityCompat.START));
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(LOG_TAG, "Die Datenquelle wird geschlossen.");
        dataSource.close();
    }

    // use switch all of day
    // if switch all of day use,
    // then time begin equal time end and
    // both show value of day
    /*function for switch button to click */
    public void activeSwitch(){
        Switch switchAllOfDay = findViewById(R.id.switchAllofDay);
        switchAllOfDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                if(isCheck==true)
                {
                    mDateSetListerner = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Log.d("all day","onDateSet: dd/mm/yyy"+ dayOfMonth + "." +(month+1)+"."+year);
                            String date = dayOfMonth + "." + (month + 1) + "." + year;

                            mDisplayDate.setText(date);
                            mDisplayTime_begin.setText(date);
                            mDisplayTime_end.setText(date);
                        }
                    };
                }
            }
        });
    }
    /*function for only checkbox Events to choice*/
    public void selectItem(){

        final CheckBox checkboxBlack = findViewById(R.id.checkboxBlack);
        final CheckBox checkboxRed = findViewById(R.id.checkboxRed);
        final CheckBox checkboxBlue = findViewById(R.id.checkboxBlue);
        final CheckBox checkboxGreen = findViewById(R.id.checkboxGreen);

                checkboxBlack.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                        if(isCheck==true)
                        {
                           checkboxRed.setChecked(false);
                           checkboxBlue.setChecked(false);
                           checkboxGreen.setChecked(false);
                        }
                    }
                });
                checkboxRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                        if(isCheck==true)
                        {
                            checkboxBlack.setChecked(false);
                            checkboxBlue.setChecked(false);
                            checkboxGreen.setChecked(false);
                        }
                    }
                });
                checkboxGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                     public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                         if(isCheck==true)
                        {
                            checkboxBlack.setChecked(false);
                            checkboxRed.setChecked(false);
                            checkboxBlue.setChecked(false);
                        }
                    }
                });
                checkboxBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                        if(isCheck==true)
                        {
                            checkboxBlack.setChecked(false);
                            checkboxRed.setChecked(false);
                            checkboxGreen.setChecked(false);
                        }
                    }
                });

            }
    /*function for Saved button to click */
    public void activateSavedButton() {
        Button buttonSaved = findViewById(R.id.calendar_saved);
        final EditText editTextTitle = findViewById(R.id.calendar_titel);
        final EditText editTextPlace = findViewById(R.id.calendar_place);
        final Button buttonDate = findViewById(R.id.calendar_date);
        final Button buttonTimeBegin = findViewById(R.id.calendar_time_begin);
        final Button buttonTimeEnd = findViewById(R.id.calendar_time_end);



        buttonSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titleString = editTextTitle.getText().toString();
                String placeString = editTextPlace.getText().toString();
                String date = buttonDate.getText().toString();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                // date_form = sdf.format(date);
                String timeBegin = buttonTimeBegin.getText().toString();
                String timeEnd = buttonTimeEnd.getText().toString();
                boolean isAllDay= false;

                CheckBox checkboxBlack = findViewById(R.id.checkboxBlack);
                CheckBox checkboxRed = findViewById(R.id.checkboxRed);
                CheckBox checkboxBlue = findViewById(R.id.checkboxBlue);
                CheckBox checkboxGreen = findViewById(R.id.checkboxGreen);

                Switch switchAllofDay = findViewById(R.id.switchAllofDay);

                int colorID=0;

                if (TextUtils.isEmpty(titleString)) {
                    editTextTitle.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(placeString)) {
                    editTextPlace.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(date)) {
                    buttonDate.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(timeBegin)) {
                    buttonTimeBegin.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                if (TextUtils.isEmpty(timeEnd)) {
                    buttonTimeEnd.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                if(switchAllofDay.isChecked())
                {
                    isAllDay=true;
                    timeEnd=timeBegin=date;
                }
                if(checkboxBlack.isChecked())
                {
                    checkboxBlue.setChecked(false);
                    checkboxRed.setChecked(false);
                    checkboxGreen.setChecked(false);
                    colorID=0;
                    Log.d(LOG_TAG, "Datenquelle wird geöffnet");
                    dataSource.open();

                    dataSource.createAppointment(date, timeBegin, timeEnd, isAllDay, titleString, placeString, colorID);

                    Log.d(LOG_TAG, "Datenquelle wird geschlossen");
                    dataSource.close();
                }
                if (checkboxRed.isChecked())
                {
                    checkboxBlue.setChecked(false);
                    checkboxBlack.setChecked(false);
                    checkboxGreen.setChecked(false);
                    colorID=1;
                    Log.d(LOG_TAG, "Datenquelle wird geöffnet");
                    dataSource.open();

                    dataSource.createAppointment(date, timeBegin, timeEnd, isAllDay, titleString, placeString, colorID);

                    Log.d(LOG_TAG, "Datenquelle wird geschlossen");
                    dataSource.close();
                }
                if (checkboxBlue.isChecked())
                {
                    checkboxBlack.setChecked(false);
                    checkboxRed.setChecked(false);
                    checkboxGreen.setChecked(false);
                    colorID=2;
                    Log.d(LOG_TAG, "Datenquelle wird geöffnet");
                    dataSource.open();

                    dataSource.createAppointment(date, timeBegin, timeEnd, isAllDay, titleString, placeString, colorID);

                    Log.d(LOG_TAG, "Datenquelle wird geschlossen");
                    dataSource.close();
                }
                if (checkboxGreen.isChecked())
                {
                    checkboxBlue.setChecked(false);
                    checkboxRed.setChecked(false);
                    checkboxBlack.setChecked(false);
                    colorID = 3;
                    Log.d(LOG_TAG, "Datenquelle wird geöffnet");
                    dataSource.open();

                    dataSource.createAppointment(date, timeBegin, timeEnd, isAllDay, titleString, placeString, colorID);

                    Log.d(LOG_TAG, "Datenquelle wird geschlossen");
                    dataSource.close();
                }

                HomeCollection.date_collection_arr=new ArrayList<HomeCollection>();
                HomeCollection.date_collection_arr.add( new HomeCollection("2019-02-25" ,titleString,"Holiday",placeString));

                editTextTitle.setText("");
                editTextPlace.setText("");
                buttonDate.setText("");
                buttonTimeBegin.setText("");
                buttonTimeEnd.setText("");

                InputMethodManager inputMethodManager;
                inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }

            }

        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        //noinspection SimplifiableIfStatement

        switch (item.getItemId()) {
            case R.id.calendar_search:
                Intent switchCalendarSearchIntent;
                switchCalendarSearchIntent = new Intent(CalendarActivity.this, CalendarSearchActivity.class);
                switchCalendarSearchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(switchCalendarSearchIntent);
                break;
            case R.id.calendar_list:
                Intent switchCalendarViewIntent;
                switchCalendarViewIntent = new Intent(CalendarActivity.this, CalendarListActivity.class);
                switchCalendarViewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(switchCalendarViewIntent);
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

}
// Menu aktiv
