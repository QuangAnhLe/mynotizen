package com.example.quangale.mynotizen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import static com.example.quangale.mynotizen.CalendarActivity.LOG_TAG;

public class CalendarListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_list);

        //show all db entries
        Log.d(LOG_TAG, "Folgende Einträge sind in der Datenbank vorhanden:");
        showAllListEntries();
    }

//--------------------------------------------------------------------------------------------------

    /**
     * Show appointments as a list view.
     * At begin data source will openafter that data source get all db entries and save it into an object list.
     * To show the objects the list will handed over to the recyclerview adapter.
     */
    private void showAllListEntries() {
        Log.d(LOG_TAG, "Datenquelle wird geöffnet");
        dataSource.open();

        List<AppointmentsObject> appointmentsObjectList = dataSource.getAllAppointments();

        dataSource.close();

        recyclerView = findViewById(R.id.appointmentRecyclerView);

        aAdapter = new AppointmentListAdapter(appointmentsObjectList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(aAdapter);
    }

    /**
     * Deletes appointments from the list view.
     * At begin data source will open, after that function to delete the appointment will called.
     */
    protected void deleteAppointment(AppointmentsObject _appointment) {
        Log.d(LOG_TAG, "Datenquelle wird geöffnet");
        dataSource = new AppointmentsDataSource(this);

        //TODO stürzt ab (wie erneut öffnen?)
        dataSource.open();

        Log.d(LOG_TAG, _appointment.toString());
        dataSource.deleteAppointment(_appointment);

    }


    //create variables
    public static final String LOG_TAG = CalendarActivity.class.getSimpleName(); //Log tag for filtering
    private AppointmentsDataSource dataSource = new AppointmentsDataSource(this);

    private RecyclerView recyclerView;
    private AppointmentListAdapter aAdapter;

}
