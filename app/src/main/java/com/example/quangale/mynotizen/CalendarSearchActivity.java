package com.example.quangale.mynotizen;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class CalendarSearchActivity extends AppCompatActivity {

    public static final String LOG_TAG = CalendarActivity.class.getSimpleName(); //Log tag for filtering
    private AppointmentsDataSource dataSource;

    private RecyclerView recyclerView;
    private AppointmentListAdapter aAdapter;


    SearchView searchView;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_search);
        searchView = findViewById(R.id.searchView);
        searchView.setQueryHint("Enter search");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String _query) {

                Log.d(LOG_TAG, "Gesucht " + _query);

//                searchDatabase(query);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String _query) {


                searchDatabase(_query);
                return false;
            }
        });
    }


    //--------------------------------------------------
    private void searchDatabase(String _query) {
        if(_query.length() > 1) {
            //Instanciate a new dataSource-object
            dataSource = new AppointmentsDataSource(this);

            Log.d(LOG_TAG, "Datenquelle wird geöffnet");
            dataSource.open();

            List<AppointmentsObject> appointmentsObjectList = dataSource.searchAppointments(_query);

            recyclerView = findViewById(R.id.appointmentSearchRecyclerView);

            aAdapter = new AppointmentListAdapter(appointmentsObjectList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(aAdapter);
        }
    }
}