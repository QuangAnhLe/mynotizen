/**
 * Connect to the database
 */
package com.example.quangale.mynotizen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class AppointmentsDataSource{

    public AppointmentsDataSource(Context context) {
        Log.d(LOG_TAG, "DataSource erzeugt jetzt den dbHelper.");
        dbHelper = new AppointmentsDbHelper(context);
    }

    //Open an database connection
    public void open() {
        Log.d(LOG_TAG, "Eine Referenz auf die Datenbank wird jetzt angefragt.");
        database = dbHelper.getWritableDatabase();

        Log.d(LOG_TAG, "Datenbank-Referenz erhalten. Pfad zur Datenbank: " + database.getPath());
    }

    //Close the database connection
    public void close() {
        dbHelper.close();
        Log.d(LOG_TAG, "Datenbank mit Hilfe des DbHelpers geschlossen.");
    }


//--------------------------------------------------------------------------------------------------

    /**
     * Save appointment into database.
     * After that the entry is save into database, a query search the saved entry to save
     * it into a cursor. Converts the cursor into appointment object to return it.
     *
     * @param  _date  an date to see when an appointment
     * @param  _dateBegin an date to see when an appointment (time)
     * @param  _dateEnd an date to see when an appointment (time)
     * @param  _isAllDay an boolean that marks if a appointment is all day
     * @param  _title the title of the appointment
     * @param  _place where the appointment is
     * @param  _colorId 0 = Black, 1 = Red, 2 = Blue
     * @return      an appointment object
     */
    public AppointmentsObject createAppointment(String _date, String _dateBegin, String _dateEnd, boolean _isAllDay, String _title, String _place, int _colorId) {
        ContentValues values = new ContentValues();
        values.put(AppointmentsDbHelper.COLUMN_DATE,_date);
        values.put(AppointmentsDbHelper.COLUMN_DATE_BEGIN, _dateBegin);
        values.put(AppointmentsDbHelper.COLUMN_DATE_END, _dateEnd);
        values.put(AppointmentsDbHelper.COLUMN_IS_ALL_DAY, _isAllDay ? 1 : 0); //Convert bool to int
        values.put(AppointmentsDbHelper.COLUMN_TITLE, _title);
        values.put(AppointmentsDbHelper.COLUMN_PLACE, _place);
        values.put(AppointmentsDbHelper.COLUMN_COLORID, _colorId);

        //Get the id of the row
        long insertId = database.insert(AppointmentsDbHelper.TABLE_APPOINTMENTS, null, values);

        Cursor cursor = database.query(AppointmentsDbHelper.TABLE_APPOINTMENTS,
                columns, AppointmentsDbHelper.COLUMN_ID + "=" + insertId,
                null, null, null, null);

        cursor.moveToFirst();
        AppointmentsObject appointmentsObject = cursorToAppointment(cursor);
//        appointmentsObject.setId(insertId);
        cursor.close();

        return appointmentsObject;
    }


//--------------------------------------------------------------------------------------------------
    //returns all objects from db
    public List<AppointmentsObject> getAllAppointments() {
        List<AppointmentsObject> appointmentsObjectList = new ArrayList<>();

        Cursor cursor = database.query(AppointmentsDbHelper.TABLE_APPOINTMENTS,
                columns, null, null, null, null, null);

        cursor.moveToFirst();
        AppointmentsObject appointmentsObject;

        while(!cursor.isAfterLast()) {
            appointmentsObject = cursorToAppointment(cursor);
            appointmentsObjectList.add(appointmentsObject);
            //Log.d(LOG_TAG, "ID: " + appointmentsObject.getId() + ", Inhalt: " + appointmentsObject.toString());
            cursor.moveToNext();
        }

        cursor.close();

        return appointmentsObjectList;
    }

    //----------------------------------------------------------------------

    //returns all objects from db
    public List<AppointmentsObject> searchAppointments(String _query) {
        List<AppointmentsObject> appointmentsObjectList = new ArrayList<>();

        String SQL_QUERY = "SELECT * FROM " + AppointmentsDbHelper.TABLE_APPOINTMENTS + " WHERE " + AppointmentsDbHelper.COLUMN_TITLE + " LIKE '%" + _query + "%'";

        Cursor cursor = database.rawQuery(SQL_QUERY, null);

        cursor.moveToFirst();
        AppointmentsObject appointmentsObject;

        while(!cursor.isAfterLast()) {
            appointmentsObject = cursorToAppointment(cursor);
            appointmentsObjectList.add(appointmentsObject);
            cursor.moveToNext();
        }

        cursor.close();

        return appointmentsObjectList;
    }

    //----------------------------------------------------------------------

    public void deleteAppointment(AppointmentsObject _appointment) {
        Log.d("         APPOINTMENT", "LÖSCHEN");

        database.delete(AppointmentsDbHelper.TABLE_APPOINTMENTS,
                AppointmentsDbHelper.COLUMN_ID + "=" + _appointment.getId(),
                null);

        Log.d(LOG_TAG, "Eintrag gelöscht! " + _appointment.toString());
    }

//--------------------------------------------------------------
    private AppointmentsObject cursorToAppointment(Cursor cursor) {
        int idID         = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_ID);
        int idDate       = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_DATE);
        int idDateBegin  = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_DATE_BEGIN);
        int idDateEnd    = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_DATE_END);
        int idIsAllDay   = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_IS_ALL_DAY);
        int idTitle      = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_TITLE);
        int idPlace      = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_PLACE);
        int idColorId    = cursor.getColumnIndex(AppointmentsDbHelper.COLUMN_COLORID);

        long id          = cursor.getLong(idID);
        String date      =cursor.getString(idDate);
        String dateBegin = cursor.getString(idDateBegin);
        String dateEnd   = cursor.getString(idDateEnd);
        boolean isAllDay = cursor.getInt(idIsAllDay) > 0;
        String title     = cursor.getString(idTitle);
        String place     = cursor.getString(idPlace);
        int colorId      = cursor.getInt(idColorId);

        AppointmentsObject appointmentsObject = new AppointmentsObject(id, date, dateBegin, dateEnd, isAllDay, title, place, colorId);

        return appointmentsObject;
    }

    //----------------------------------------------------------------
    private static final String LOG_TAG = AppointmentsDataSource.class.getSimpleName();

    private SQLiteDatabase database;
    private AppointmentsDbHelper dbHelper;

    private String[] columns = {
            AppointmentsDbHelper.COLUMN_ID,
            AppointmentsDbHelper.COLUMN_DATE,
            AppointmentsDbHelper.COLUMN_DATE_BEGIN,
            AppointmentsDbHelper.COLUMN_DATE_END,
            AppointmentsDbHelper.COLUMN_IS_ALL_DAY,
            AppointmentsDbHelper.COLUMN_TITLE,
            AppointmentsDbHelper.COLUMN_PLACE,
            AppointmentsDbHelper.COLUMN_COLORID,
    };
}